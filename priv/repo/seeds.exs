alias Weirdo.{Accounts.User, Repo}

Repo.delete_all(User)

now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)

1..1_000_000
|> Enum.map(fn num -> %{id: num, inserted_at: now, updated_at: now} end)
|> Enum.chunk_every(20_000)
|> Enum.each(&Repo.insert_all(User, &1))
