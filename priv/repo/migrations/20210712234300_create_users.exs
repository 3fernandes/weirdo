defmodule Weirdo.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :points, :integer, default: 0

      timestamps()
    end

    create constraint("users", :points_range, check: "points>=0 and points<=100")
    create index(:users, [:points])
  end
end
