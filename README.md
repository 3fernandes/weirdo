# Weirdo

### Requirements
Well, the idea was to build a simple Phoenix API, with a single endpoint that should return 2 users(at most) with more `points` than the current state of `max_number`, on each/every request.

1. The `points` column in the Users table, should accept any number between `0` and `100`;

2. It should generate a million(`1_000_000`) users from the seeds file, and each with `0` points;

3. When the app starts, a `genserver` should be launched with 2 elements as the state;

    - A random number from [0 - 100], which I called `max_number`;
    - A `timestamp` which indicates the last time someone queried the server, and starts as `nil`;

4. It should run on every minute, and when it runs:

    - It should update every user's `points` in the DB(using a random number from 0 - 100 for each);
    - Refresh the `max_number` of the `genserver` state with a new random number;

5. On every API call, it should:

    - Query the DB for all **users** with more `points` than the `max_number`, but only retrieve a max of 2 **users**;
    - Updates the `timestamp` of `genserver` state, with the timestamp of the last time it was queried;
    - The API response must contain, the **users** retrieved from the DB, as well as the `timestamp` of the `genserver` state;

#
### Usage

1. Use the same Erlang/Elixir versions used during the development, just to make sure we're on the same page and avoid any sort of deprecated modules/functions on incompatibilities. And I really hope you're using `asdf` to manage your runtime versions, if so, please run:

  ```sh
  asdf install
  ```

2. To download the used libs, create the DB, migrate the DB structure and seed some data(1.000.000 Users 😱) to play with, please run:

  ```sh
  mix setup
  ```

3. To run the tests(and YES, we got some coverage 🥳), please run:

  ```sh
  mix test
  ```

4. To start the application and have it running on port 4000(_But if you're feeling fancy, you can do you and run on your desired port_), please run:

  ```sh
  mix phx.server
  ```

5. Now, use your favorite REST Client and call the following path:

  ```sh
  http://localhost:4000/
  ```

6. Now, you shall see a magic like the following:

![Screenshot_2023-03-02_at_00.51.25](/uploads/23cd4ef56863fdeca3b55040c18fe2de/Screenshot_2023-03-02_at_00.51.25.png)

- And YES, fancy folks like me use [Insomnia](https://insomnia.rest/) 🤗✨

#
### Contributing

It is not necessary. As the name implies, it's really an app with some weird requirements and I don't recommend you contribute, unless you want to have fun like I had fun building it. 🤪

#
### Words of Wisdom

There is an old saying, by a well unknown computer expert:

> **It works on my machine...**
>
> by Mr. Unknown
