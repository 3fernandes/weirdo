defmodule Weirdo.AccountTest do
  use Weirdo.DataCase, async: true

  alias Weirdo.{Accounts, Accounts.User}

  describe "get_users_with_max_points/1" do
    setup do
      for x <- 0..5 do
        User.changeset(%User{}, %{points: x}) |> Repo.insert()
      end
    end

    test "should retrieve 2 users with more :points than max_number(1) arg" do
      max_number = 1

      users = Accounts.get_users_with_max_points(max_number)

      assert 2 = length(users)
    end

    test "should retrieve 1 user with more :points than max_number(4) arg" do
      max_number = 4

      users = Accounts.get_users_with_max_points(max_number)

      assert 1 = length(users)
    end

    test "should not retrieve any users, since there's no users with 100 :points or more" do
      max_number = 100

      users = Accounts.get_users_with_max_points(max_number)

      assert [] = users
    end
  end

  describe "update_users_with_random_points/0" do
    test "should update all the existent users" do
      {:ok, user_before_update} = User.changeset(%User{}, %{points: 0}) |> Repo.insert()

      Accounts.update_users_with_random_points()

      user_after_update = Repo.get(User, user_before_update.id)

      assert user_before_update.points != user_after_update.points
    end
  end
end
