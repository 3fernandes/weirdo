defmodule Weirdo.WeirdoStatefulServerTest do
  use ExUnit.Case

  alias Weirdo.{Repo, WeirdoStatefulServer}

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Repo, {:shared, self()})

    pid =
      %{
        id: WeirdoStatefulServerTest,
        start: {WeirdoStatefulServer, :start_link, [[name: WeirdoStatefulServerTest]]}
      }
      |> start_supervised!()

    %{pid: pid}
  end

  test "should return a nil timestamp and an empty list of users on the first call" do
    assert {:ok, %{users: _users, timestamp: nil}} =
             WeirdoStatefulServer.get_users(WeirdoStatefulServerTest)
  end

  test "should not return a nil timestamp on the second call" do
    {:ok, _} = WeirdoStatefulServer.get_users(WeirdoStatefulServerTest)

    refute match?(
             {:ok, %{timestamp: nil, users: _users}},
             WeirdoStatefulServer.get_users(WeirdoStatefulServerTest)
           )
  end

  test "process should be able to recover from unexpected exits", %{pid: pid} do
    Process.exit(pid, :normal)
    Process.sleep(1)

    assert {:ok, _} = WeirdoStatefulServer.get_users(WeirdoStatefulServerTest)
  end
end
