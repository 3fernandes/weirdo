defmodule Weirdo.Accounts.UserTest do
  use Weirdo.DataCase

  alias Ecto.Changeset
  alias Weirdo.Accounts.User

  describe "changeset/2" do
    test "changeset should return error when invalid params(like points: 101) are provided" do
      params = %{points: 101}

      changeset = User.changeset(%User{}, params)

      refute changeset.valid?

      expected_error = {"is invalid", [validation: :inclusion, enum: 0..100]}
      assert %Changeset{errors: [points: ^expected_error]} = changeset
    end

    test "changeset should return error when invalid params(like points: -1) are provided" do
      params = %{points: -1}

      changeset = User.changeset(%User{}, params)

      refute changeset.valid?

      expected_error = {"is invalid", [validation: :inclusion, enum: 0..100]}
      assert %Changeset{errors: [points: ^expected_error]} = changeset
    end

    test "changeset should be valid when valid params(like points: 0..100) are provided" do
      params = %{points: 100}

      changeset = User.changeset(%User{}, params)

      assert changeset.valid?
      assert %Changeset{errors: []} = changeset
    end
  end
end
