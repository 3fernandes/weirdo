defmodule WeirdoWeb.RootControllerTest do
  use WeirdoWeb.ConnCase

  describe "Index" do
    test "should return a nil timestamp and a list of users for the first call", %{conn: conn} do
      conn = get(conn, Routes.root_path(conn, :index))

      assert %{"timestamp" => nil, "users" => _users} = json_response(conn, 200)
    end

    test "should return a timestamp and a list of users for the second call", %{conn: conn} do
      get(conn, Routes.root_path(conn, :index))
      conn = get(conn, Routes.root_path(conn, :index))

      refute match?(%{"timestamp" => nil, "users" => _users}, json_response(conn, 200))
    end
  end
end
