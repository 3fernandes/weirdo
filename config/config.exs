# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :weirdo,
  ecto_repos: [Weirdo.Repo]

# Configures the endpoint
config :weirdo, WeirdoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "lSSJ1elNKMzNTHM7oHUGIWFgt21k9i7xiES36ETM31CISmK8dxuhA+Vdu3Xj4sg3",
  render_errors: [view: WeirdoWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Weirdo.PubSub,
  live_view: [signing_salt: "tg4OhXtf"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
