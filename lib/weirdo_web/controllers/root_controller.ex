defmodule WeirdoWeb.RootController do
  use WeirdoWeb, :controller

  alias Weirdo.WeirdoStatefulServer

  def index(conn, _params) do
    {:ok, users_and_timestamp} = WeirdoStatefulServer.get_users()

    render(conn, "index.json", users_and_timestamp: users_and_timestamp)
  end
end
