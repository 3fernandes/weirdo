defmodule WeirdoWeb.RootView do
  use WeirdoWeb, :view

  def render("index.json", %{users_and_timestamp: users_and_timestamp}) do
    %{
      users: users_and_timestamp.users,
      timestamp: users_and_timestamp.timestamp
    }
  end
end
