defmodule Weirdo.WeirdoStatefulServer do
  use GenServer

  alias Weirdo.Accounts

  @refresh_time 60_000

  defstruct [:max_number, :timestamp]

  # Client API

  def start_link(opts \\ []) do
    name = Keyword.get(opts, :name, __MODULE__)
    GenServer.start_link(__MODULE__, %__MODULE__{max_number: gen_random_number()}, name: name)
  end

  def get_users(name \\ __MODULE__) do
    GenServer.call(name, :get_users)
  end

  # Server callbacks

  def init(state) do
    refresh_after_1min()

    {:ok, state}
  end

  def handle_info(:refresh, state) do
    Accounts.update_users_with_random_points()

    refresh_after_1min()

    {:noreply, %__MODULE__{state | max_number: gen_random_number()}}
  end

  def handle_call(:get_users, _from, %{max_number: max_number, timestamp: timestamp} = state) do
    users = Accounts.get_users_with_max_points(max_number)

    users_and_timestamp = %{users: users, timestamp: timestamp}

    {:reply, {:ok, users_and_timestamp}, %__MODULE__{state | timestamp: current_date_time()}}
  end

  defp gen_random_number do
    Enum.random(0..100)
  end

  defp refresh_after_1min do
    Process.send_after(self(), :refresh, @refresh_time)
  end

  defp current_date_time do
    NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  end
end
