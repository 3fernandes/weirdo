defmodule Weirdo.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false

  alias Weirdo.{Accounts.User, Repo}

  def get_users_with_max_points(max_number) do
    from(u in User, where: u.points > ^max_number, limit: 2)
    |> Repo.all()
  end

  def update_users_with_random_points do
    update(
      User,
      set: [
        points: fragment("floor(random()*100)"),
        updated_at: fragment("now()")
      ]
    )
    |> Repo.update_all([])
  end
end
