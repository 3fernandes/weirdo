defmodule Weirdo.Repo do
  use Ecto.Repo,
    otp_app: :weirdo,
    adapter: Ecto.Adapters.Postgres
end
