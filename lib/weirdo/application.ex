defmodule Weirdo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start a worker by calling: Weirdo.Worker.start_link(arg)
      # {Weirdo.Worker, arg}
      Weirdo.WeirdoStatefulServer,
      # Start the Ecto repository
      Weirdo.Repo,
      # Start the Telemetry supervisor
      WeirdoWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Weirdo.PubSub},
      # Start the Endpoint (http/https)
      WeirdoWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Weirdo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    WeirdoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
